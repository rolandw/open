# open

## Background

Open is a bash script to open a document with the default application. If you pass the -a flag and a partial name of an app, it will search through /usr/share/applications or ~/.local/share/applications for the first desktop file matching that name, will extract the executable path from that file and will attempt to open your document with that app. In that way you can open that html file with your default browser, with another browser, with your IDE or text-editor or even with GIMP.


Open was first written when I moved from OS X to Linux and discovered that the equivalent "open" command opened a new virtual terminal and wasn't what was needed. "xdg-open" works fine but only uses the default app. I wanted to be able to specify an app to open my document with.

## Installation

At the moment there is no deb/rpm or similar. Clone the project (```git clone git@gitlab.com:rolandw/open.git```). cd to the project directory and run ```chmod +x open```. Put a symlink to the file into a directory in your path. From experience, I generally use /usr/local/bin. If you want the man page installed, cd to your man path (usually /usr/local/share/man/man1) and add a symlink to the open.1.gz file in the project directory. Then run ```makewhatis``` to update the man file cache.

## Usage

```open [-v][-a <application>] <file>```

-v turns on verbose mode - generally for debugging. You might not like the messages but they work for me.

-a <application> will attempt to open the file with the application named.

-h will print the help messages.

### Examples

``` open mypdf.pdf```

will open mypdf in your dedault PDF viewer


``` open myproject/index.html ```

will open the index page of your project with your default browser.


``` open -a firefox myproject/index.html ```

will open the index page of your project using Firefox (er, surely your default browser?)


``` open -a emacs myproject/index.html ```

will open the index page of your project in Emacs ready for you to edit it.


## Testing

This script has been tested against Ubuntu 20. You might need to make changes if you are running on a different distribution.
