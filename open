#!/bin/bash

# Written by roland <roland@chart-shed.com>
# Last modified: 5th November 2020

showhelp() {
    echo "open - open a file using a specific or default app"
    echo "usage: $open [-v][-a <application>] <file>"
    echo ""
    echo "       This script has been tested against Ubuntu 20. You might need to make changes if"
    echo "       you are running on a different distribution."
    echo ""
    echo "       try flag -v for debug/verbose mode."
    echo ""
    echo "example: open downloads/mydocument.pdf"

    if [[ $verbose ]]; then
	echo ""
	echo "You are running in debug mode"
    echo "Note that the -a option searches the .desktop files in /usr/share/applicaions and in"
    echo "~/.local/share.applications and uses the first found application with the name provided."
    fi

    exit 1
}

findapp() {
    if [ $# -eq 0 ]; then
        result="Error: no application provided"
    else
       res=$(find /usr/share/applications ~/.local/share/applications -iname "*$1*" -exec grep -i exec {} \; | head -1 | cut -d "=" -f 2 | cut -d " " -f 1)
       if [ $? -ne 0 ]; then
          result="Error finding application: $res"
       elif [ -z $res ] ; then
           result="Error: application $1 not found"
       else
           result=$res
       fi
    fi
    echo $result
}


while getopts ":a:vh?" opts; do
    case "${opts}" in
	    h|\?)
	        showhelp
	        ;;
	    v)
	        verbose=True
	        ;;
	    a)
	        customApp=True
            applicn=${OPTARG}
            executable=$(findapp "$applicn")
            ;;
    esac
done
 
shift $(($OPTIND -1))
while [[ $1 && ${1} ]]; do
    filepath=$(printf %q "${1}")
    if [ $customApp ]; then
        if [[ $executable == Error* ]] ; then
            echo $executable
        elif [ -z $executable ] ; then
            if [[ $verbose ]]; then
                echo "No such application was found"
            fi
        else
            if [[ $verbose ]] ; then
                errorHandler="2>/dev/null"
                echo "Running command: $executable $filepath $errorHandler"
            else
                errorHandler=""
            fi
            sh -c "$executable $filepath $errorHandler &"
        fi
    else
        if [[ $verbose ]]; then
            echo "About to open file $filepath with the default app"
        fi 
        xdg-open $filepath & 
    fi
    last=${1}
    shift 1
    if [ "$1" == "$last" ]; then
	break
    fi
done
